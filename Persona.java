public class Persona{
private String nombre;
private String apellidoPaterno;
private String apellidoMaterno;
private int edad;
private char sexo;
private String direccion;
private String curp;


// Constructor con Parametros
public Persona(String nombre, String apellidoPaterno, String apellidoMaterno, int edad, char sexo,
               String direccion, String curp){
               
 this.nombre = nombre; 
 this.apellidoPaterno = apellidoPaterno; 
 this.apellidoMaterno = apellidoMaterno; 
 this.edad  = edad; 
 this.sexo = sexo;
 this.direccion = direccion; 
 this.curp = curp;
}

// metodos set( asiginar)

public void setNombre(String name){
 nombre = name;
}

public void setApellidoPaterno(String Apaterno){
apellidoPaterno = Apaterno;
}

public void setApellidoMaterno(String Amaterno){
apellidoMaterno = Amaterno;
}

public void setEdad(int age){
edad = age;
}
public void setSexo(char s){
sexo = s;
}

public void setDireccion(String direccion){
this.direccion = direccion;
}

public void setCurp(String curp){
this.curp = curp;
}

// metodo get

public String getNombre(){
 return nombre;
}

public String getApellidoPaterno(){
return apellidoPaterno;
}

public String getApellidoMaterno(){
return apellidoMaterno;
}

public int getEdad(){
return edad;
}
 

public String getDireccion(){
return direccion;
}

public String getCurp(){
return curp;
}

public String getSexo(){
String sex="";
if(sexo == 'h' || sexo == 'H')
  sex = "Hombre";
 
if(sexo == 'm' || sexo == 'M')
 sex = "Mujer";   
 
return sex;  
 } 

}